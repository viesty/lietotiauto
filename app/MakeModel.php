<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MakeModel extends Model
{
    public function make() {
        return $this->belongsTo(Make::class);
    }

    public function advertisements() {
        return $this->hasMany(Advertisement::class);
    }
}
