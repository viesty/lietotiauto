<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Make extends Model
{
    public function models() {
        return $this->hasMany(MakeModel::class);
    }

    public function advertisements() {
        return $this->hasMany(Advertisement::class);
    }
}
