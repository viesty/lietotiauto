<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    public function attributeGroups() {
        return $this->hasMany(AttributeGroup::class);
    }

    public function attributeGroupValue() {
        return $this->belongsTo(AttributeGroupValue::class);
    }

    public function advertisement() {
        return $this->belongsTo(Advertisement::class);
    }
}
