<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
    public function user() {
        return $this->belongsTo(User::class);
    }

    public function attributes() {
        return $this->hasMany(Attribute::class);
    }

    public function make() {
        return $this->belongsTo(Make::class);
    }

    public function makeModel() {
        return $this->belongsTo(MakeModel::class);
    }

    public function images() {
        return $this->hasMany(Image::class);
    }
}
