<?php
use Symfony\Component\HttpFoundation\Request;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::auth();
    Route::get('/account', 'AccountController@account');
    Route::get('lang/{lang}', ['as'=>'lang.switch', 'uses'=>'LanguageController@switchLang']);
    Route::get('preference/{preference}', ['as'=>'preference.switch', 'uses'=>'AccountController@switchPreference']);

    Route::resource('/', 'IndexController');
    Route::resource('/advert', 'AdvertController');
    Route::get('catalog/{makeId}', 'CatalogController@modelList');
    Route::post('catalog/{makeId}/model/{modelId}', 'CatalogController@filterList');
    Route::get('catalog', 'CatalogController@index');
    Route::resource('catalog.model', 'CatalogController');
});

Route::get('api/dropdown', function(Request $req) {
    $input = $req->option;
    if ($input) {
        $makeModels = App\Make::find($input)->models;
        return response()->json($makeModels);
    } else {
        return null;
    }
});

Route::get('api/pngnumber', function(Request $req) {
    $user = $req->user;
    $token = $req->token;
    if($user && $token) {

        if($token == md5($user.'tokenG-12')){
            //get all adverts of the user and check if we are looking at a real one

            $img = Intervention\Image\Facades\Image::make('static/empty.png');

            $img->text(App\User::find($user)->phone, 200, 25, function($font) {
                $font->file('static/Geometria-Light.otf');
                $font->size(25);
                $font->color('#ff0000');
                $font->align('center');
                $font->valign('middle');
            });

            return $img->response();
        } else {
            return null;
        }
    }
});