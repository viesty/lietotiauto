<?php

namespace App\Http\Controllers;

use App\Attribute;
use App\AttributeGroup;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Advertisement;
use App\AttributeGroupValue;
use App\Make;
use Illuminate\Support\Facades\Input;

class CatalogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $makes = Make::all();

        return view('advert/list', array('makeList' => $makes));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function modelList($makeId)
    {
        $models = Make::find($makeId)->models;

        return view('advert/list', array('models' => $models, 'make' => $makeId));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($makeId, $makeModelId)
    {
        $adverts = Advertisement::where('enabled', '=', '1')
                                ->where('make_model_id', '=', $makeModelId)
                                ->orderBy('created_at', 'desc')->paginate(16);

        foreach($adverts as $ad) {
            $mainImage = $ad->images()->where('id', '=', $ad->main_image_id)->first();
            if($mainImage) {
                $mainImageLocation = DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.$mainImage->filename;
                $mainImageThumbnailLocation = DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'thumbnail_'.$mainImage->filename;
            } else {
                $mainImageLocation = DIRECTORY_SEPARATOR.'static'.DIRECTORY_SEPARATOR.'no-image.jpg';
                $mainImageThumbnailLocation = $mainImageLocation;
            }

            $ad->main_image_location = $mainImageLocation;
            $ad->main_image_thumbnail_location = $mainImageThumbnailLocation;
        }

        $filters = array();

        $allGroups = AttributeGroup::all();

        $i = 0;
        foreach($allGroups as $group) {
            $filters[$i] = array();

            $filters[$i]['title']   = $group->title;
            $filters[$i]['id']      = $group->id;

            $filters[$i]['values']  = array();

            $groupValues = $group->values;
            $j = 0;
            foreach($groupValues as $groupVal) {
                $filters[$i]['values'][$j]['value']  = $groupVal->value;
                $filters[$i]['values'][$j]['id']  = $groupVal->id;
                $j++;
            }
            $i++;
        }
        $makes = Make::lists('title', 'id');
        $filters = (object)($filters);
        return view('advert/list', array('ads' => $adverts, 'filters' => $filters, 'makes' => $makes, 'make_model_id' => $makeModelId));
    }

    public function filterList(Request $req)
    {
        $data = $req->all();

        $query = Advertisement::where('enabled', '=', '1');

        if(isset($data['make_model_id'])) {
            $query->where('make_model_id', '=', $data['make_model_id']);
        }

        if(isset($data['advert_type']) && strlen($data['advert_type']) > 0) {
            $advertType = $data['advert_type'];
            $query->where('advert_type', '=', $advertType);
        }

        if(isset($data['price'])) {
            $price = $data['price'];

            if($price['from'] > 0) {
                $priceFrom = $price['from'];

                $query->where('price', '>=', $priceFrom);
            }
            if($price['to'] > 0) {
                $priceTo = $price['to'];

                $query->where('price', '<=', $priceTo);
            }
        }
        if(isset($data['mileage'])) {
            $mileage = $data['mileage'];

            if($mileage['from'] > 0) {
                $mileageFrom = $mileage['from'];
                $query->where('mileage', '>=', $mileageFrom);
            }
            if($mileage['to'] > 0) {
                $mileageTo = $mileage['to'];
                $query->where('mileage', '<=', $mileageTo);
            }
        }

        $attributeResults = null;

        if(isset($data['attributes'])) {
            $attrs = $data['attributes'];

            $attributeQuery = Attribute::where('id', '>', '0');

            foreach($attrs as $group => $value) {
                if(strlen($value) > 0) {
                    $attributeQuery->where('attribute_group_id', '=', $group)->where('attribute_group_value_id', '=', $value);
                }
            }
            $attributeResults = $attributeQuery->get(['advertisement_id']);
        }

        $ids = null;
        if($attributeResults) {
            $ids = array();
            foreach($attributeResults as $result) {
                array_push($ids, $result->advertisement_id);
            }
        }

        if($ids) {
            $query->whereIn('id', $ids);
        }

        $filters = array();
        $allGroups = AttributeGroup::all();
        $i = 0;
        foreach($allGroups as $group) {
            $filters[$i] = array();

            $filters[$i]['title']   = $group->title;
            $filters[$i]['id']      = $group->id;

            $filters[$i]['values']  = array();

            $groupValues = $group->values;
            $j = 0;
            foreach($groupValues as $groupVal) {
                $filters[$i]['values'][$j]['value']  = $groupVal->value;
                $filters[$i]['values'][$j]['id']  = $groupVal->id;
                $j++;
            }
            $i++;
        }
        $result = $query->paginate(12);
        $makes = Make::lists('title', 'id');
        $filters = (object)($filters);

        foreach($result as $ad) {
            $mainImage = $ad->images()->where('id', '=', $ad->main_image_id)->first();
            if($mainImage) {
                $mainImageLocation = DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.$mainImage->filename;
                $mainImageThumbnailLocation = DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'thumbnail_'.$mainImage->filename;
            } else {
                $mainImageLocation = DIRECTORY_SEPARATOR.'static'.DIRECTORY_SEPARATOR.'no-image.jpg';
                $mainImageThumbnailLocation = $mainImageLocation;
            }

            $ad->main_image_location = $mainImageLocation;
            $ad->main_image_thumbnail_location = $mainImageThumbnailLocation;
        }

        Input::flash();
        return view('advert/list', array('ads' => $result, 'filters' => $filters, 'makes' => $makes, 'make_model_id' => $data['make_model_id']));
    }
}
