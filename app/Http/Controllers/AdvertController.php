<?php

namespace App\Http\Controllers;

use App\Make;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\User;
use App\Advertisement;
use App\Image;
use App\Attribute;
use App\AttributeGroupValue;
use Intervention;
use Storage;

class AdvertController extends Controller
{

    private $_imageLocation;
    private $_user;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->_imageLocation = 'uploads';
        $this->_user = Auth::user();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('/catalog');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(!Auth::guest()) {
            $makes = Make::lists('title', 'id');
            return view('advert/add/form', array('makes' => $makes));

        } else {
            return redirect('/login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::guest()) {
            $data = $request->all();

            $files = $request->file()['images'];

            $this->validate($request, [
                'title' => 'required|min:3|max:50',
                'description' => 'required|min:3|max:2000',
                'make' => 'required|regex:/[\d]+/',
                'model' => 'required|regex:/[\d]+/',
                'advert_type' => 'required|regex:/[\d]+/',
                'price' => 'required|regex:/[\d]+/',
                'mileage' => 'required|regex:/[\d]+/',
            ]);

            $ad = new Advertisement();
            $ad->title = $data['title'];
            $ad->description = $data['description'];
            $ad->price = $data['price'];
            $ad->mileage = $data['mileage'];
            $ad->user_id = Auth::user()->id;
            $ad->make_id = $data['make'];
            $ad->make_model_id = $data['model'];
            $ad->advert_type = $data['advert_type'];
            $ad->mileage = $data['mileage'];
            $ad->save();

            $fileRules = array(
                'file' => 'required|mimes:png,gif,jpeg'
            );

            if (isset($request->file()['main_image']) > 0) {
                $mainImage = $request->file()['main_image'];
                $validator = $this->getValidationFactory()->make(array('file' => $mainImage), $fileRules);
                if ($validator->passes()) {
                    $filename = $mainImage->getClientOriginalName();
                    $filename = preg_replace(array('/\s/', '/\.[\.]+/', '/[^\w_\.\-]/'), array('_', '.', ''), $filename);
                    $filename = $ad->id . '_' . rand(1, 100000) . '_' . $filename;
                    $mainImage->move($this->_imageLocation, $filename);
                    $image = new Image();
                    $image->mime = $mainImage->getClientMimeType();
                    $image->filename = $filename;
                    $image->advertisement_id = $ad->id;
                    $image->save();
                    $ad->main_image_id = $image->id;
                    $ad->save();

                    // generate the thumbnail
                    $thumbnailFilename = 'thumbnail_'.$filename;
                    Intervention\Image\Facades\Image::make($this->_imageLocation.DIRECTORY_SEPARATOR.$filename)->fit(200, 200, function ($constraint) {
                        $constraint->upsize();
                    })->save($this->_imageLocation.DIRECTORY_SEPARATOR.$thumbnailFilename);
                }
            }

            if (sizeof($files) > 0) {
                foreach ($files as $file) {
                    $validator = $this->getValidationFactory()->make(array('file' => $file), $fileRules);

                    if ($validator->passes()) {
                        $filename = $file->getClientOriginalName();
                        $filename = preg_replace(array('/\s/', '/\.[\.]+/', '/[^\w_\.\-]/'), array('_', '.', ''),
                            $filename);
                        $filename = $ad->id . '_' . rand(1, 100000) . '_' . $filename;
                        $file->move($this->_imageLocation, $filename);
                        $image = new Image();
                        $image->mime = $file->getClientMimeType();
                        $image->filename = $filename;
                        $image->advertisement_id = $ad->id;
                        $image->save();

                        // generate the thumbnail
                        $thumbnailFilename = 'thumbnail_'.$filename;
                        Intervention\Image\Facades\Image::make($this->_imageLocation.DIRECTORY_SEPARATOR.$filename)->fit(200, 200, function ($constraint) {
                            $constraint->upsize();
                        })->save($this->_imageLocation.DIRECTORY_SEPARATOR.$thumbnailFilename);
                    }
                }
            }

            $toUnset = array('title', 'description', 'price', 'mileage', 'make', 'model', 'advert_type', 'images');

            foreach ($toUnset as $key) {
                unset($data[$key]);
            }

            foreach ($data as $k => $v) {
                preg_match("/attr-group-(\d+)/", $k, $match);
                if (sizeof($match) > 1) {
                    $adAttr = new Attribute();
                    $adAttr->attribute_group_id = $match[1];
                    $adAttr->attribute_group_value_id = $v;
                    $adAttr->advertisement_id = $ad->id;
                    $adAttr->save();
                }
            }

            return redirect('/advert/' . $ad->id);
        } else {
            return redirect('/');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $canEdit = false;
        $advert = Advertisement::findOrFail($id);
        $advertAttributes = $advert->attributes;

//        dd($advertAttributes);

        if($this->_user && ($this->_user->id == $advert->user_id || $this->_user->isAdmin())) $canEdit = true;

        $additonalAttributes = array();

        $i = 0;
        foreach($advertAttributes as $attr) {
            $agv = AttributeGroupValue::find($attr->attribute_group_value_id);
            $additonalAttributes[$i] = array();
            $additonalAttributes[$i]['title'] = $agv->group->title;
            $additonalAttributes[$i]['value'] = $agv->value;
            $i++;
        }

        $imageArray = array();
        $images = $advert->images;
        $i = 0;
        if(count($images) > 0) {
            foreach($images as $img) {
                if($img->id != $advert->main_image_id) {
                    $imageArray[$i] = array();
                    $imageArray[$i]['image'] = DIRECTORY_SEPARATOR.$this->_imageLocation.DIRECTORY_SEPARATOR.$img->filename;
                    $imageArray[$i]['thumb'] = DIRECTORY_SEPARATOR.$this->_imageLocation.DIRECTORY_SEPARATOR.'thumbnail_'.$img->filename;
                    $i++;
                }
            }
        }

        $attributeObject = (object)$additonalAttributes;
        $imageObject = (object)$imageArray;

        $mainImageLocation = null;
        $mainImage = $advert->images()->where('id', '=', $advert->main_image_id)->first();
        if($mainImage) {
            $mainImageLocation = DIRECTORY_SEPARATOR.$this->_imageLocation.DIRECTORY_SEPARATOR.$mainImage->filename;
        }

        return view('advert/view', array('advert' => $advert, 'additional' => $attributeObject, 'images' => $imageObject, 'mainImg' => $mainImageLocation, 'canEdit' => $canEdit));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ad = Advertisement::findOrFail($id);
        if(($this->_user && $ad) && ($ad->user_id == $this->_user->id || $this->_user->isAdmin())) {
            $makes = Make::lists('title', 'id');
            $mainImageLocation = null;
            $mainImageId = 0;
            $mainImage = $ad->images()->where('id', '=', $ad->main_image_id)->first();
            if($mainImage) {
                $mainImageLocation = DIRECTORY_SEPARATOR.$this->_imageLocation.DIRECTORY_SEPARATOR.$mainImage->filename;
                $mainImageId = $mainImage->id;
            }

            $imageArray = array();
            $images = $ad->images;
            $i = 0;
            if(count($images) > 0) {
                foreach($images as $img) {
                    if($img->id != $ad->main_image_id) {
                        $imageArray[$i] = array();
                        $imageArray[$i]['id'] = $img->id;
                        $imageArray[$i]['image'] = DIRECTORY_SEPARATOR.$this->_imageLocation.DIRECTORY_SEPARATOR.$img->filename;
                        $imageArray[$i]['thumb'] = DIRECTORY_SEPARATOR.$this->_imageLocation.DIRECTORY_SEPARATOR.'thumbnail_'.$img->filename;
                        $i++;
                    }
                }
            }
            $imageObject = (object)$imageArray;

            return view('advert/edit/form', array('advertisement' => $ad, 'makes' => $makes, 'make_id' => $ad->make_id, 'make_model_id' => $ad->make_model_id, 'mainImg' => $mainImageLocation, 'images' => $imageObject, 'main_image_id' => $mainImageId));
        }

        return redirect('advert/'.$id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ad = Advertisement::findOrFail($id);
        if(($this->_user && $ad) && ($ad->user_id == $this->_user->id || $this->_user->isAdmin())) {
            $data = $request->all();

            $mainImageId = $ad->main_image_id;
            if(isset($data['delete-advert-image']) && count($data['delete-advert-image']) > 0) {
                foreach($data['delete-advert-image'] as $imgId) {
                    if($imgId == $mainImageId) {
                        $ad->main_image_id = null;
                        $ad->save();
                    }

                    if(($imgObj = Image::find($imgId)) && $imgObj->advertisement_id == $ad->id) {
                        Storage::disk('uploads')->delete('thumbnail_'.$imgObj->filename);
                        Storage::disk('uploads')->delete($imgObj->filename);
                        $imgObj->delete();
                    }
                }
            }

            $files = $request->file()['images'];
            $this->validate($request, [
                'title' => 'required|min:3|max:50',
                'description' => 'required|min:3|max:2000',
                'make' => 'required|regex:/[\d]+/',
                'model' => 'required|regex:/[\d]+/',
                'advert_type' => 'required|regex:/[\d]+/',
                'price' => 'required|regex:/[\d]+/',
                'mileage' => 'required|regex:/[\d]+/',
            ]);

            $ad->title = $data['title'];
            $ad->description = $data['description'];
            $ad->price = $data['price'];
            $ad->mileage = $data['mileage'];
            $ad->user_id = Auth::user()->id;
            $ad->make_id = $data['make'];
            $ad->make_model_id = $data['model'];
            $ad->advert_type = $data['advert_type'];
            $ad->mileage = $data['mileage'];
            $ad->save();

            $fileRules = array(
                'file' => 'required|mimes:png,gif,jpeg'
            );

            if (isset($request->file()['main_image']) > 0) {
                $mainImage = $request->file()['main_image'];
                $validator = $this->getValidationFactory()->make(array('file' => $mainImage), $fileRules);
                if ($validator->passes()) {
                    $filename = $mainImage->getClientOriginalName();
                    $filename = preg_replace(array('/\s/', '/\.[\.]+/', '/[^\w_\.\-]/'), array('_', '.', ''), $filename);
                    $filename = $ad->id . '_' . rand(1, 100000) . '_' . $filename;
                    $mainImage->move($this->_imageLocation, $filename);
                    $image = new Image();
                    $image->mime = $mainImage->getClientMimeType();
                    $image->filename = $filename;
                    $image->advertisement_id = $ad->id;
                    $image->save();
                    $ad->main_image_id = $image->id;
                    $ad->save();

                    // generate the thumbnail
                    $thumbnailFilename = 'thumbnail_'.$filename;
                    Intervention\Image\Facades\Image::make($this->_imageLocation.DIRECTORY_SEPARATOR.$filename)->fit(200, 200, function ($constraint) {
                        $constraint->upsize();
                    })->save($this->_imageLocation.DIRECTORY_SEPARATOR.$thumbnailFilename);
                }
            }

            if (sizeof($files) > 0) {
                foreach ($files as $file) {
                    $validator = $this->getValidationFactory()->make(array('file' => $file), $fileRules);

                    if ($validator->passes()) {
                        $filename = $file->getClientOriginalName();
                        $filename = preg_replace(array('/\s/', '/\.[\.]+/', '/[^\w_\.\-]/'), array('_', '.', ''),
                            $filename);
                        $filename = $ad->id . '_' . rand(1, 100000) . '_' . $filename;
                        $file->move($this->_imageLocation, $filename);
                        $image = new Image();
                        $image->mime = $file->getClientMimeType();
                        $image->filename = $filename;
                        $image->advertisement_id = $ad->id;
                        $image->save();

                        // generate the thumbnail
                        $thumbnailFilename = 'thumbnail_'.$filename;
                        Intervention\Image\Facades\Image::make($this->_imageLocation.DIRECTORY_SEPARATOR.$filename)->fit(200, 200, function ($constraint) {
                            $constraint->upsize();
                        })->save($this->_imageLocation.DIRECTORY_SEPARATOR.$thumbnailFilename);
                    }
                }
            }

            $toUnset = array('title', 'description', 'price', 'mileage', 'make', 'model', 'advert_type', 'images');

            foreach ($toUnset as $key) {
                unset($data[$key]);
            }

            foreach ($data as $k => $v) {
                preg_match("/attr-group-(\d+)/", $k, $match);
                if (sizeof($match) > 1) {
                    $adAttr = Attribute::where('attribute_group_id', '=', $match[1])->where('advertisement_id', '=', $id)->first();
                    $adAttr->attribute_group_value_id = $v;
                    $adAttr->advertisement_id = $ad->id;
                    $adAttr->save();
                }
            }
        }

        return redirect('advert/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ad = Advertisement::findOrFail($id);
        if ((!Auth::guest() && $ad) && $this->_user->isAdmin()) {
            if($ad->enabled) $ad->enabled = false;
            else $ad->enabled = true;
            $ad->save();
        }

        return redirect('/');
    }
}
