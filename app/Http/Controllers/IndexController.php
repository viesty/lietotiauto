<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Advertisement;
use App\Make;
use App\AttributeGroup;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $preference = 0;

        if(!Auth::guest()) {
            $preference = Auth::user()->homepage_display;
        }
        if($preference == 0) {
            $adverts = Advertisement::where('enabled', '=', '1')->orderBy('created_at', 'desc')->get()->take(12);
        } else {
            $adverts = Advertisement::where('enabled', '=', '1')->where('user_id', '=', Auth::user()->id)->orderBy('created_at', 'desc')->paginate(12);
        }

        foreach($adverts as $ad) {
            $mainImage = $ad->images()->where('id', '=', $ad->main_image_id)->first();
            if($mainImage) {
                $mainImageLocation = DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.$mainImage->filename;
                $mainImageThumbnailLocation = DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'thumbnail_'.$mainImage->filename;
            } else {
                $mainImageLocation = DIRECTORY_SEPARATOR.'static'.DIRECTORY_SEPARATOR.'no-image.jpg';
                $mainImageThumbnailLocation = $mainImageLocation;
            }

            $ad->main_image_location = $mainImageLocation;
            $ad->main_image_thumbnail_location = $mainImageThumbnailLocation;
        }

        return view('index', array('ads' => $adverts, 'preference' => $preference));
    }
}
