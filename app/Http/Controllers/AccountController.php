<?php

namespace App\Http\Controllers;

use App\Advertisement;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    private $_user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->_user = Auth::user();
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function account()
    {
        $ads = $this->_user->advertisements()->paginate(12);

        foreach($ads as $ad) {
            $mainImage = $ad->images()->where('id', '=', $ad->main_image_id)->first();
            if($mainImage) {
                $mainImageLocation = DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.$mainImage->filename;
                $mainImageThumbnailLocation = DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'thumbnail_'.$mainImage->filename;
            } else {
                $mainImageLocation = DIRECTORY_SEPARATOR.'static'.DIRECTORY_SEPARATOR.'no-image.jpg';
                $mainImageThumbnailLocation = $mainImageLocation;
            }

            $ad->main_image_location = $mainImageLocation;
            $ad->main_image_thumbnail_location = $mainImageThumbnailLocation;
        }
        return view('account', array('user' => $this->_user, 'ads' => $ads));
    }

    public function switchPreference($pref)
    {
        if($pref == 0 || $pref == 1) {
            $this->_user->homepage_display = $pref;
            $this->_user->save();
        }

        return redirect()->back();
    }
}
