<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttributeGroup extends Model
{
    public function values() {
        return $this->hasMany(AttributeGroupValue::class, 'group_id');
    }

    public function attribute() {
        return $this->belongsTo(Attribute::class);
    }

    static function getAllValues($id, $showEmpty = false) {
        if($showEmpty) {
            return [''=>'Any'] + AttributeGroupValue::where('group_id', '=', $id)->get()->lists('value', 'id')->all();
        }

        return AttributeGroupValue::where('group_id', '=', $id)->get()->lists('value', 'id');
    }
}
