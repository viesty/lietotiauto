<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttributeGroupValue extends Model
{
    public function group() {
        return $this->belongsTo(AttributeGroup::class);
    }

    public function attributes() {
        return $this->hasMany(Attribute::class);
    }
}
