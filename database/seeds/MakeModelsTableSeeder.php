<?php

use Illuminate\Database\Seeder;

class MakeModelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            '1' => array('title' => 'Audi', 'make_logo' => 'audi.png', 'models' => array(
                0 =>
                    array (
                        'title' => '100',
                    ),
                1 =>
                    array (
                        'title' => '200',
                    ),
                2 =>
                    array (
                        'title' => '4000',
                    ),
                3 =>
                    array (
                        'title' => '5000',
                    ),
                4 =>
                    array (
                        'title' => '80',
                    ),
                5 =>
                    array (
                        'title' => '90',
                    ),
                6 =>
                    array (
                        'title' => 'A3',
                    ),
                7 =>
                    array (
                        'title' => 'A4',
                    ),
                8 =>
                    array (
                        'title' => 'A5',
                    ),
                9 =>
                    array (
                        'title' => 'A6',
                    ),
                10 =>
                    array (
                        'title' => 'A7',
                    ),
                11 =>
                    array (
                        'title' => 'A8',
                    ),
            )),
            '2' => array('title' => 'BMW', 'make_logo' => 'bmw.png','models' => array(
                0 =>
                    array (
                        'title' => '1 Series (3)',
                    ),
                1 =>
                    array (
                        'title' => '128i',
                    ),
                2 =>
                    array (
                        'title' => '135i',
                    ),
                3 =>
                    array (
                        'title' => '135is',
                    ),
                4 =>
                    array (
                        'title' => '3 Series (29)',
                    ),
                5 =>
                    array (
                        'title' => '318i',
                    ),
                6 =>
                    array (
                        'title' => '318iC',
                    ),
                7 =>
                    array (
                        'title' => '318iS',
                    ),
                8 =>
                    array (
                        'title' => '318ti',
                    ),
                9 =>
                    array (
                        'title' => '320i',
                    ),
                10 =>
                    array (
                        'title' => '323ci',
                    ),
                11 =>
                    array (
                        'title' => '323i',
                    ),
                12 =>
                    array (
                        'title' => '323is',
                    ),
                13 =>
                    array (
                        'title' => '323iT',
                    ),
                14 =>
                    array (
                        'title' => '325Ci',
                    ),
                15 =>
                    array (
                        'title' => '325e',
                    ),
                16 =>
                    array (
                        'title' => '325es',
                    ),
                17 =>
                    array (
                        'title' => '325i',
                    ),
            )),
            '3' => array('title' => 'Volkswagen', 'make_logo' => 'volkswagen.png','models' => array(
                0 =>
                    array (
                        'title' => 'Beetle',
                    ),
                1 =>
                    array (
                        'title' => 'Cabrio',
                    ),
                2 =>
                    array (
                        'title' => 'Cabriolet',
                    ),
                3 =>
                    array (
                        'title' => 'CC',
                    ),
                4 =>
                    array (
                        'title' => 'Corrado',
                    ),
                5 =>
                    array (
                        'title' => 'Dasher',
                    ),
                6 =>
                    array (
                        'title' => 'Eos',
                    ),
                7 =>
                    array (
                        'title' => 'Eurovan',
                    ),
                8 =>
                    array (
                        'title' => 'Fox',
                    ),
                9 =>
                    array (
                        'title' => 'GLI',
                    ),
                10 =>
                    array (
                        'title' => 'Golf R',
                    ),
                11 =>
                    array (
                        'title' => 'GTI',
                    ),
                12 =>
                    array (
                        'title' => 'Golf and Rabbit Models (2)',
                    ),
                13 =>
                    array (
                        'title' => 'Golf',
                    ),
                14 =>
                    array (
                        'title' => 'Rabbit',
                    ),
                15 =>
                    array (
                        'title' => 'Jetta',
                    ),
            )),
        );

        foreach($data as $d) {
            DB::table('makes')->insert(array('title' => $d['title'], 'make_logo' => $d['make_logo']));
            $lastId = DB::getPdo()->lastInsertId();

            foreach($d['models'] as $model) {
                DB::table('make_models')->insert(array('title' => $model['title'], 'make_id' => $lastId));
            }
        }
    }
}
