<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            '1' => array('name' => 'Viesturs Ruzans', 'email' => 'viesturs.ruzans@gmail.com', 'password' => bcrypt('password'), 'class' => 2, 'phone' => '26161523'),
        );

        foreach($data as $d) {
            DB::table('users')->insert($d);
        }
    }
}
