<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(AttributeGroupsTableSeeder::class);
        $this->call(AttributeGroupValuesTableSeeder::class);
        $this->call(MakeModelsTableSeeder::class);
        $this->call(AdvertisementTableSeeder::class);
        $this->call(AttributesTableSeeder::class);
    }
}
