<?php

use Illuminate\Database\Seeder;

class AttributeGroupValuesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            '1' => array('group_id' => '1', 'value' => 'Melna'),
            '2' => array('group_id' => '1', 'value' => 'Balta'),
            '3' => array('group_id' => '2', 'value' => 'Dīzelis'),
            '4' => array('group_id' => '2', 'value' => 'Benzīns'),
            '5' => array('group_id' => '3', 'value' => 'Priekšējā'),
            '55' => array('group_id' => '3', 'value' => 'Aizmugurējā'),
            '6' => array('group_id' => '4', 'value' => 'Kreisajā'),
            '7' => array('group_id' => '4', 'value' => 'Labajā'),
            '9' => array('group_id' => '5', 'value' => 'Automātiskā'),
            '99' => array('group_id' => '5', 'value' => 'Manuālā'),
            '10' => array('group_id' => '3', 'value' => 'Viss'),
        );

        foreach($data as $d) {
            DB::table('attribute_group_values')->insert($d);
        }
    }
}
