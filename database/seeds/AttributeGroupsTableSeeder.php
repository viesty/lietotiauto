<?php

use Illuminate\Database\Seeder;

class AttributeGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            '1' => array('title' => 'Krāsa'),
            '2' => array('title' => 'Degvielas tips'),
            '3' => array('title' => 'Piedziņa'),
            '4' => array('title' => 'Stūre'),
            '5' => array('title' => 'Ātrumkārbas tips'),
        );

        foreach($data as $d) {
            DB::table('attribute_groups')->insert($d);
        }
    }
}
