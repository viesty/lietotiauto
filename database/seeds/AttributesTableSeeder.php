<?php

use Illuminate\Database\Seeder;

class AttributesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            '1' => array('advertisement_id' =>  1, 'attribute_group_id' =>  1, 'attribute_group_value_id' => 2),
            '2' => array('advertisement_id' =>  1, 'attribute_group_id' =>  2, 'attribute_group_value_id' => 4),
            '3' => array('advertisement_id' =>  1, 'attribute_group_id' =>  3, 'attribute_group_value_id' => 5),
            '4' => array('advertisement_id' =>  1, 'attribute_group_id' =>  5, 'attribute_group_value_id' => 10),
            '5' => array('advertisement_id' =>  2, 'attribute_group_id' =>  1, 'attribute_group_value_id' => 1),
            '6' => array('advertisement_id' =>  3, 'attribute_group_id' =>  5, 'attribute_group_value_id' => 10),
            '7' => array('advertisement_id' =>  4, 'attribute_group_id' =>  1, 'attribute_group_value_id' => 2),
            '8' => array('advertisement_id' =>  4, 'attribute_group_id' =>  4, 'attribute_group_value_id' => 7),
            '9' => array('advertisement_id' =>  5, 'attribute_group_id' =>  1, 'attribute_group_value_id' => 1),
            '10' => array('advertisement_id' => 5, 'attribute_group_id' =>  2, 'attribute_group_value_id' => 3),
        );

        foreach($data as $d) {
            DB::table('attributes')->insert($d);
        }
    }
}
