<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mime');
            $table->string('filename');
            $table->timestamps();
            $table->integer('advertisement_id')->unsigned()->nullable();
            $table->foreign('advertisement_id')->references('id')->on('advertisements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('images', function($table)
        {
            $table->dropForeign('images_advertisement_id_foreign');
        });

        Schema::drop('images');
    }
}
