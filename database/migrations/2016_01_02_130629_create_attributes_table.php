<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('attribute_group_id')->unsigned();
            $table->foreign('attribute_group_id')->references('id')->on('attribute_groups');
            $table->integer('attribute_group_value_id')->unsigned();
            $table->foreign('attribute_group_value_id')->references('id')->on('attribute_group_values');
            $table->integer('advertisement_id')->unsigned();
            $table->foreign('advertisement_id')->references('id')->on('advertisements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attributes', function($table)
        {
            $table->dropForeign('attributes_attribute_group_id_foreign');
            $table->dropForeign('attributes_attribute_group_value_id_foreign');
            $table->dropForeign('attributes_advertisement_id_foreign');
        });
        Schema::drop('attributes');
    }
}
