<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertisementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertisements', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('title');
            $table->string('description');
            $table->integer('price');
            $table->integer('mileage');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('make_id')->unsigned();
            $table->foreign('make_id')->references('id')->on('makes');
            $table->integer('make_model_id')->unsigned();
            $table->foreign('make_model_id')->references('id')->on('make_models');
            $table->integer('advert_type')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('advertisements', function($table)
        {
            $table->dropForeign('advertisements_make_id_foreign');
            $table->dropForeign('advertisements_make_model_id_foreign');
            $table->dropForeign('advertisements_user_id_foreign');
        });
        Schema::drop('advertisements');
    }
}
