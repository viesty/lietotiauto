@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('trans.Hello') }}, {{ $user->name }}</div>
                <div class="panel-body">
                    <h3>{{ trans('trans.Change your language') }}</h3>
                    @foreach (Config::get('languages') as $lang => $language)
                        @if ($lang != App::getLocale())
                            <li>
                                <a href="{{ route('lang.switch', $lang) }}">{{$language}}</a>
                            </li>
                        @endif
                    @endforeach
                    <h3>{{ trans('trans.Change your preferred home page content') }}</h3>
                    @if($user->homepage_display == 1)
                        <li><a href="{{ route('preference.switch', 0) }}">Newest advertisements</a></li>
                    @else
                        <li><a href="{{ route('preference.switch', 1) }}">Your advertisements</a></li>
                    @endif
                    <h3>{{ trans('trans.Your advertisements') }}</h3>
                    @include('layouts/car_card')
                    {!! $ads->render() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
