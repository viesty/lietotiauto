@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    @if($preference == 0)
                        {{ trans('trans.Newest Ads') }}
                    @else
                        {{ trans('trans.Your Ads') }}
                    @endif
                </div>
                <div class="panel-body">
                    @include('layouts/car_card')
                    @if($preference == 1)
                        {!! $ads->render() !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
