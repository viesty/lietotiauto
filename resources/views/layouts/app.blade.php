<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Lietotiau.to</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.2.13/css/lightgallery.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('style/stylesheets/style.css') }}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.2.13/js/lightgallery-all.js"></script>

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Navigācija</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}">
                    Lietotiau.to
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <ul class="nav navbar-nav">
                    @if(!Auth::guest())
                        <li><a href="{{ url('/account') }}">{{ trans('trans.Profile') }}</a></li>
                        <li><a href="{{ url('/advert/create') }}">{{ trans('trans.Add an Advertisement') }}</a></li>
                    @endif
                    <li><a href="{{ url('/catalog') }}">{{ trans('trans.Catalog') }}</a></li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">{{ trans('trans.Login') }}</a></li>
                        <li><a href="{{ url('/register') }}">{{ trans('trans.Register') }}</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

    <script>
        jQuery(document).ready(function($){

            var makeField = $('#make');

            if(makeField.val() != undefined && makeField.val().length > 0) {
                makeFieldChange(makeField);
            }

            if(makeField) {
                makeField.change(function() {
                    makeFieldChange(makeField);
                });
            }

            $('#advert_type').change(function() {
                var priceLabel = $('#price-label');
                if($(this).val() == '2') {
                    priceLabel.text('Price (per month)');
                } else {
                    priceLabel.text('Price');
                }
            });
        });

        function makeFieldChange(makeField) {
            var model = $('#make_model');

            if(makeField.val().length > 0) {
                model.attr('disabled', 'disabled');
                $.get("{{ url('api/dropdown')}}",
                        {
                            option: makeField.val()
                        },
                        function(data) {
                            model.empty();

                            $.each(data, function(index, element) {
                                model.append("<option value='"+ element.id +"'>" + element.title + "</option>");
                            });
                            model.removeAttr('disabled');

                            if(model.data('selectId')) {
                                model.find('option[value="'+model.data('selectId')+'"]').attr('selected', 'selected');
                            }
                        });

            } else {
                model.empty();
                model.append("<option value=''>Select a make first!</option>");
                model.attr('disabled', 'disabled');
            }
        }
    </script>
</body>
</html>
