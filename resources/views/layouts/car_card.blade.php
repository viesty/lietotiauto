<div class="car-card-container">
    @foreach($ads as $ad)
        <a href="{{ url('/advert/'.$ad->id) }}" class="car-card @if(!$ad->enabled) disabled @endif">
            <div class="image">
                <img src="{{ $ad->main_image_thumbnail_location }}" alt="main-image">
            </div>
            <div class="info">
                <h3>{{ $ad->title }}</h3>
            </div>
        </a>
    @endforeach
</div>