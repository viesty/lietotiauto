@if($filters)
    {!! Form::open(array('url'=>Request::url(),'method'=>'POST', 'class' => 'form', 'id' => 'advert-submit-form')) !!}
    {!! csrf_field() !!}

    <div class="form-group">
        <label class="col-md-5 control-label">Advertisement type</label>

        <div class="col-md-7">
            {!! Form::select('advert_type', array('' => 'Any', '0' => 'Sale', '1' => 'Exchange', '2' => 'Loan'), null, ['class' => 'form-control', 'id' => 'advert_type']) !!}
            @if(isset($make_model_id))
                {{ Form::hidden('make_model_id', $make_model_id) }}
            @endif
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-5 control-label" id="price-label">Price From</label>

        <div class="col-md-7">
            {!! Form::number('price[from]', old('price[from]'), ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-5 control-label" id="price-label">Price To</label>

        <div class="col-md-7">
            {!! Form::number('price[to]', '', ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-5 control-label">Mileage From</label>

        <div class="col-md-7">
            {!! Form::number('mileage[from]', '', ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-5 control-label">Mileage To</label>

        <div class="col-md-7">
            {!! Form::number('mileage[to]', '', ['class' => 'form-control']) !!}
        </div>
    </div>

    @foreach(App\AttributeGroup::all() as $attrGroup)
        <div class="form-group">
            <label class="col-md-5 control-label">{{ $attrGroup->title }}</label>

            <div class="col-md-7">
                {!! Form::select("attributes[$attrGroup->id]", App\AttributeGroup::getAllValues($attrGroup->id, true), null, ['class' => 'form-control']) !!}
            </div>
        </div>
    @endforeach

    <div class="form-group">
        <div class="col-md-8 col-md-offset-4">
            <button type="submit" class="btn btn-primary">
                <i class="fa fa-btn fa-plus-square-o"></i>Search
            </button>
        </div>
    </div>
    {!! Form::close() !!}
@endif