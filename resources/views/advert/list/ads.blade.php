@if(isset($ads))
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">{{ trans('trans.All Ads') }}</div>
            <div class="panel-body">
                @include('layouts/car_card')
            </div>
            {!! $ads->render() !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading">{{ trans('trans.Search') }}</div>
            <div class="panel-body">
                @include('layouts/search')
            </div>
        </div>
    </div>
@endif
