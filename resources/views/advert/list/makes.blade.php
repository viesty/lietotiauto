@if(isset($makeList))
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">{{ trans('trans.Choose a make') }}</div>
            <div class="panel-body make-list">
                @foreach($makeList as $make)
                    <a href="{{ url('/catalog/'.$make->id) }}" class="make">
                        @if($make->make_logo)
                            <img src="/static/{{$make->make_logo}}" alt="{{ $make->title }}">
                        @else
                            {{ $make->title }}
                        @endif
                    </a>
                @endforeach
            </div>
        </div>
    </div>
@endif