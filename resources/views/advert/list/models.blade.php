@if(isset($models))
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">{{ trans('trans.Choose a model') }}</div>
            <div class="panel-body model-list">
                @foreach($models as $model)
                    <a href="{{ url('/catalog/'.$make.'/model/'.$model->id) }}" class="model">
                        {{ $model->title }}
                    </a>
                @endforeach
            </div>
        </div>
    </div>
@endif