@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Add an advertisement</div>
                    <div class="panel-body">
                        {!! Form::open(array('url'=>'/advert','method'=>'POST', 'files'=>true, 'class' => 'form', 'id' => 'advert-submit-form')) !!}
                            {!! csrf_field() !!}

                            <div class="form-group">
                                <label class="col-md-4 control-label">Car Make</label>

                                <div class="col-md-8">
                                    {!! Form::select('make', $makes, null, ['class' => 'form-control', 'id' => 'make']) !!}
                                </div>
                            </div>
                            @if ($errors->has('make'))
                                <div class="row">
                                    <span class="alert alert-danger col-md-12">
                                        <strong>{{ $errors->first('make') }}</strong>
                                    </span>
                                </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-4 control-label">Car Model</label>

                                <div class="col-md-8">
                                    {!! Form::select('model', array('' => 'Select a make first!'), 0, ['class' => 'form-control', 'id' => 'make_model', 'disabled']) !!}
                                </div>
                            </div>
                            @if ($errors->has('model'))
                                <div class="row">
                                    <span class="alert alert-danger col-md-12">
                                        <strong>{{ $errors->first('model') }}</strong>
                                    </span>
                                </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-4 control-label">Advertisement type</label>

                                <div class="col-md-8">
                                    {!! Form::select('advert_type', array('0' => 'I am selling the car', '1' => 'I am exchanging the car', '2' => 'I am loaning the car'), 0, ['class' => 'form-control', 'id' => 'advert_type']) !!}
                                </div>
                            </div>
                            @if ($errors->has('advert_type'))
                                <div class="row">
                                    <span class="alert alert-danger col-md-12">
                                        <strong>{{ $errors->first('advert_type') }}</strong>
                                    </span>
                                </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-4 control-label">Title</label>

                                <div class="col-md-8">
                                    {!! Form::text('title', '', ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            @if ($errors->has('title'))
                                <div class="row">
                                    <span class="alert alert-danger col-md-12">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-4 control-label">Description</label>

                                <div class="col-md-8">
                                    {!! Form::textarea('description', '', ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            @if ($errors->has('description'))
                                <div class="row">
                                    <span class="alert alert-danger col-md-12">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-4 control-label" id="price-label">Price</label>

                                <div class="col-md-8">
                                    {!! Form::number('price', '', ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            @if ($errors->has('price'))
                                <div class="row">
                                    <span class="alert alert-danger col-md-12">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-4 control-label">Mileage</label>

                                <div class="col-md-8">
                                    {!! Form::number('mileage', '', ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            @if ($errors->has('mileage'))
                                <div class="row">
                                    <span class="alert alert-danger col-md-12">
                                        <strong>{{ $errors->first('mileage') }}</strong>
                                    </span>
                                </div>
                            @endif

                            @foreach(App\AttributeGroup::all() as $attrGroup)
                                <div class="form-group">
                                    <label class="col-md-4 control-label">{{ $attrGroup->title }}</label>

                                    <div class="col-md-8">
                                        {!! Form::select("attr-group-$attrGroup->id", App\AttributeGroup::getAllValues($attrGroup->id), null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                @if ($errors->has("attr-group-$attrGroup->id"))
                                    <div class="row">
                                    <span class="alert alert-danger col-md-12">
                                        <strong>{!! $errors->first("attr-group-$attrGroup->id") !!}</strong>
                                    </span>
                                    </div>
                                @endif
                            @endforeach

                            <div class="form-group">
                                <label class="col-md-4 control-label">Main image</label>

                                <div class="col-md-8">
                                    {!! Form::file('main_image') !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Additional images</label>

                                <div class="col-md-8">
                                    {!! Form::file('images[]', array('multiple'=>true)) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-plus-square-o"></i>Add
                                    </button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
