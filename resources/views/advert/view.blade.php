@extends('layouts.app')

@section('content')
    <div class="container advert-view">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ $advert->title }}
                        @if($canEdit)
                            <a href="/advert/{{ $advert->id }}/edit" class="pull-right btn btn-warning ad-edit-button">Edit</a>
                        @endif
                        @if(!Auth::guest() && Auth::user()->isAdmin())
                            {{ Form::open(array('url'=> '/advert/'.$advert->id, 'method' => 'delete', 'class' => 'advert-hide-form')) }}
                                @if($advert->enabled)
                                    <button type="submit" class="pull-right btn btn-warning">Hide</button>
                                @else
                                    <button type="submit" class="pull-right btn btn-warning">Show</button>
                                @endif
                            {{ Form::close() }}
                        @endif
                    </div>
                    <div class="panel-body">
                        <div class="errors">
                            @foreach($errors->all() as $message)
                                <p class="alert alert-danger">{!! $message !!}</p>
                            @endforeach
                        </div>
                        <div class="row">
                            @if(!$advert->enabled)
                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="alert alert-warning">This advertisement is hidden!</p>
                                    </div>
                                </div>
                            @endif
                            <div class="col-md-5">
                                <div class="panel-body image-panel">
                                    @if($images)
                                        <div class="advert-images">
                                            <div id="lightgallery">
                                                @if($mainImg)
                                                    <a class="main-image" href="{{ $mainImg }}">
                                                        <img src="{{ $mainImg }}" />
                                                    </a>
                                                @endif
                                                @foreach($images as $img)
                                                    <a href="{{ $img['image'] }}">
                                                        <img src="{{ $img['thumb'] }}" />
                                                    </a>
                                                @endforeach
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="panel-heading car-model">{{ $advert->make->title }} {{ $advert->makeModel->title }}</div>
                                <div class="panel-body">
                                    <div class="row">
                                        <label class="col-md-2 control-label">{{ trans('trans.Description') }}</label>
                                        <div class="col-md-10">
                                            {{ $advert->description }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-md-2 control-label">{{ trans('trans.Mileage') }}</label>
                                        <div class="col-md-10">
                                            {{ $advert->mileage }}
                                        </div>
                                    </div>
                                    @foreach($additional as $attr)
                                        <div class="row">
                                            <label class="col-md-2 control-label">{{ $attr['title'] }}</label>
                                            <div class="col-md-10">
                                                {{ $attr['value'] }}
                                            </div>
                                        </div>
                                    @endforeach
                                    <div class="row price-row">
                                        <p class="col-md-12">{{ trans('trans.Price') }} @if($advert->advert_type == 2)<span class="small">/month</span>@endif <span class="price">&euro; {{ $advert->price }}</span></p>
                                    </div>
                                    <div class="row">
                                        <label class="col-md-2 control-label">{{ trans('trans.Added by') }}</label>
                                        <div class="col-md-10">
                                            {{ $advert->user->name }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-md-2 control-label">{{ trans('trans.Date') }}</label>
                                        <div class="col-md-10">
                                            {{ $advert->created_at }}
                                        </div>
                                    </div>
                                    @if($advert->user->phone)
                                        <div class="row">
                                            <label class="col-md-2 control-label">{{ trans('trans.Phone') }}</label>
                                            <div class="col-md-10">
                                                <img src="/api/pngnumber?user={{ $advert->user->id }}&token={{ md5($advert->user->id.'tokenG-12') }}" alt="">
                                            </div>
                                        </div>
                                    @endif
                                    @if($advert->user->email)
                                        <div class="row">
                                            <label class="col-md-2 control-label">{{ trans('trans.Email') }}</label>
                                            <div class="col-md-10">
                                                {{ $advert->user->email }}
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#lightgallery").lightGallery();
        });
    </script>
@endsection
