@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('advert/list/makes')
            @include('advert/list/models')
            @include('advert/list/ads')
        </div>
    </div>
@endsection
