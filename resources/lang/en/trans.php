<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Misc Language Lines
    |--------------------------------------------------------------------------
    */

    'Newest Ads' => 'Newest Ads',
    'Search' => 'Search',
    'Hello' => 'Hello',
    'Your advertisements' => 'Your advertisements',
    'Change your language' => 'Change your language',
    'Profile' => 'Profile',
    'Add an Advertisement' => 'Add an Advertisement',
    'Catalog' => 'Catalog',
    'Login' => 'Login',
    'Register' => 'Register',
    'Hide' => 'Hide',
    'Edit' => 'Edit',
    'Description' => 'Description',
    'Mileage' => 'Mileage',
    'Price' => 'Price',
    'Added by' => 'Added by',
    'Date' => 'Date',
    'Email' => 'Email',
    'Phone' => 'Phone',
    'Color' => 'Color',
    'Choose a make' => 'Choose a make',
    'Choose a model' => 'Choose a model',
    'All Ads' => 'All Ads',
    'Your Ads' => 'Your Ads',
    'Change your preferred home page content' => 'Change your preferred home page content',

];
