<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Parolei jābūt vismaz 6 simbolus garai.',
    'reset' => 'Parole atiestatīta!',
    'sent' => 'Mēs jums uz epastu nosūtījām paroles atiestatīšanas saiti!',
    'token' => 'Paroles atiestatīšanas atslēga nav pareiza.',
    'user' => "Mēs nevarējām atrast lietotāju ar šādu epastu.",

];
