<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Misc Language Lines
    |--------------------------------------------------------------------------
    */

    'Newest Ads' => 'Jaunākie sludinājumi',
    'Search' => 'Meklētājs',
    'Hello' => 'Sveiks',
    'Your advertisements' => 'Tavi sludinājumi',
    'Change your language' => 'Valodas maiņa',
    'Profile' => 'Profils',
    'Add an Advertisement' => 'Pievienot sludinājumu',
    'Catalog' => 'Katalogs',
    'Login' => 'Ielogoties',
    'Register' => 'Reģistrēties',
    'Hide' => 'Paslēpt',
    'Edit' => 'Rediģēt',
    'Description' => 'Apraksts',
    'Mileage' => 'Nobraukums',
    'Price' => 'Cena',
    'Added by' => 'Pievienoja',
    'Date' => 'Datums',
    'Email' => 'Epasts',
    'Phone' => 'Tel.',
    'Color' => 'Krāsa',
    'Choose a make' => 'Izvēlies ražotāju',
    'Choose a model' => 'Izvēlies modeli',
    'All Ads' => 'Visi sludinājumi',
    'Your Ads' => 'Tavi sludinājumi',
    'Change your preferred home page content' => 'Pamaini sev vēlamo mājaslapas saturu',

];
